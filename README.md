# Sugar App

[Production Link]
  - Instant search/results
  - View sugar content in terms of the number of teaspoons of granulated sugar and number of circus peanuts (lol)
  - Massive food database thanks to the USDA

Sugar is a lightweight web app designed for you to quickly and easily **reveal how much sugar is in almost any food in the form of *circus peanuts* and *teaspoons* of granulated sugar**. 

Sugar is a web-based mobile-ready, AngularJS powered HTML5 interface to the USDA's Nutrient Database for Standard Reference (NDSR) API, and was inspired by the idea behind the [#ShowUsYourPeanuts] hashtag as seen on Last Week Tonight, hosted by  John Oliver.

In action: 
![alt text](https://crispyappstudiosblog.files.wordpress.com/2015/11/sugar-1.gif?w=464&h=300 "A search for Cheerios")

### Version
0.2.0

### Tech

Sugar uses a number of open source projects to work properly:

* [AngularJS] - HTML enhanced for web apps!
* [Twitter Bootstrap] - great UI boilerplate for modern web apps
* [jQuery] - Bootstrap dependency

And of course Sugar itself is open source with a [public repository]
 on GitHub.
 
### Formulas
* Number of circus peanuts = (grams of sugar / 7)
* Number of teaspoons of granulated sugar  = (grams of sugar / 4)
 
### Disclaimer
This app is currently under development and may not return completely accurate data in every instance, and intended as a guide only. For guaranteed accuracy use the USDA's database: http://ndb.nal.usda.gov/ndb/foods  
To get the amount of granulated sugar in teaspoons, divide (grams of sugar / 4).

### Development

Want to contribute? Devs welcome!

Just clone the source code, and serve the directory with your favorite HTTP server and start writing!

### Todos

 - Modularize code into controllers/views following Angular best practices
 - Improve the interface to eliminate scrolling (possibly add animations using ng-animate)
 - Reformat get requests into a factory
 - Write Tests
 - Implement toast so it is triggered on loading/done, rather than statically called on request completion 
 - (make toast update dynamically using .updateContent()
 - Add search suggestions/autocomplete to seach bar
 - Show an image of a circus peanut per peanut
 - Logo/favicon (I'm thinking a heaping teaspoon spoon full of sugar, or circus peanut depending on the title)

License
----

[GNU Affero GPL 3]

Special Thanks
---
[Micah Bolen], 
[jm____], 
[Anik Abhi]

###[Dev Blog]




   [public repository]: <https://bitbucket.org/nickberry17/sugar>
   [Twitter Bootstrap]: <http://twitter.github.com/bootstrap/>
   [Production Link]: <http://nickberry17.bitbucket.org/sugar/index.html/>
   [jQuery]: <http://jquery.com>
   [AngularJS]: <http://angularjs.org>
   [USDA Databse]: <http://ndb.nal.usda.gov/ndb/foods>
   [Anik Abhi]: <https://stackoverflow.com/questions/33492342/how-to-hide-an-angular-expression-if-null-when-there-is-math-inside-the-expressi/33492571#33492571>
   [jm____]: <https://stackoverflow.com/questions/33458411/how-to-pass-json-response-to-model-for-use-in-get-request/33458526?noredirect=1#comment54703630_33458526>
   [Micah Bolen]: <https://github.com/micahbolen/>
   [#ShowUsYourPeanuts]: <https://twitter.com/search?q=%23showusyourpeanuts&src=typd>
   [GNU Affero GPL 3]:<https://www.gnu.org/licenses/agpl-3.0.en.html>
   [Dev Blog]: <https://crispyappstudiosblog.wordpress.com/2015/11/05/sugar-showusyourpeanuts-working-title/
>